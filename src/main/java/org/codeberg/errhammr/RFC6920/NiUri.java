/*
 * RFC6920-Java - a Minimalistic Implementation of RFC 6920 in Java
 * Copyright (C) 2023  errhammr
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License only.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.codeberg.errhammr.RFC6920;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * Implementation of RFC 6920
 */
public class NiUri {

    public static void main(String[] args) {
        if(args.length < 2 || args.length % 2 != 0) {
            printUsage();
            System.exit(1);
        }

        String file = null;
        String str = null;
        String uriString = null;
        String algo = "sha-256";
        String authority = "";
        boolean nih = false;
        String protocol = null;
        boolean relative = false;

        int argc = 0;
        while(argc < args.length) {
            switch(args[argc]) {
                case "--file":
                case "-f":
                    file = args[argc +1];
                    break;
                case "--string":
                case "-s":
                    str = args[argc +1];
                    break;
                case "--uri":
                case "-u":
                    uriString = args[argc +1];
                    break;
                case "--algo":
                    algo = args[argc +1];
                    break;
                case "--authority":
                case "-a":
                    authority = args[argc +1];
                    break;
                case "--type":
                case "-t":
                    if("ni".equals(args[argc +1])) {
                        // nothing to do for ni:// URIs
                        break;
                    }
                    if("nih".equals(args[argc +1])) {
                        nih = true;
                    }
                    else {
                        protocol = args[argc +1];
                    }
                    break;
                case "--relative":
                    switch (args[argc +1].toLowerCase()) {
                        case "yes":
                        case "true":
                        case "y":
                        case "on":
                        case "enable":
                        case "enabled":
                            relative = true;
                    }
                    break;
                default:
                    System.err.println("Unknown argument: " + args[argc]);
                    printUsage();
                    System.exit(2);
            }

            argc += 2;
        }

        NiUri niUri = null;

        if(file != null) {
            try {
                niUri = NiUri.ofFile(file, algo, authority);
            } catch (IOException e) {
                System.err.println("Unable to read file: " + e.getMessage());
                System.exit(3);
            } catch (NoSuchAlgorithmException e) {
                System.err.println("Unknown algorithm: " + e.getMessage());
                System.exit(4);
            }
        }
        else if(str != null) {
            try {
                niUri = NiUri.ofString(str, algo, authority);
            } catch (NoSuchAlgorithmException e) {
                System.err.println("Unknown algorithm: " + e.getMessage());
                System.exit(4);
            }
        }
        else if(uriString != null) {
            try {
                niUri = NiUri.ofUri(uriString);
            } catch (NoSuchAlgorithmException e) {
                System.err.println("Unknown algorithm: " + e.getMessage());
                System.exit(4);
            }
        }
        else {
            System.err.println("Missing argument: either --file or --string need to be provided");
            System.exit(2);
        }

        if(nih) {
            System.out.println(niUri.formatNih(relative));
        }
        else if (protocol != null) {
            if((niUri.getAuthority() == null || "".equals(niUri.getAuthority())) && authority != null && !"".equals(authority))
            {
                System.out.println(niUri.formatWellKnown(protocol, authority));
            }
            else {
                System.out.println(niUri.formatWellKnown(protocol));
            }
        }
        else {
            if((niUri.getAuthority() == null || "".equals(niUri.getAuthority())) && authority != null && !"".equals(authority)) {
                System.out.println(niUri.format(authority, relative));
            }
            else {
                System.out.println(niUri.format(relative));
            }
        }
    }

    private static void printUsage() {
        System.err.println("Usage: NiUri.java (--file FILE | --string STRING | --uri URI)");
        System.err.println("                  [--algo ALGO] [--authority AUTHORITY]");
        System.err.println("                  [--type nih | WELL_KNOWN_PROTOCOL]");
        System.err.println("                  [--relative RELATIVE]");
        System.err.println("  FILE                 path to a file to hash, mutually exclusive with --string");
        System.err.println("                       and --uri");
        System.err.println("  STRING               plain text string to hash, mutually exclusive with --file");
        System.err.println("                       and --uri");
        System.err.println("  URI                  ni or nih URI to parse, mutually exclusive with --file");
        System.err.println("                       and --string");
        System.err.println("  ALGO                 sha-256, sha-384 or sha-512, default: sha-256");
        System.err.println("  AUTHORITY            the authority to use in the URI, optional");
        System.err.println("  WELL_KNOWN_PROTOCOL  the protocol to use when crating a well-known URL,");
        System.err.println("                       optional");
        System.err.println("  RELATIVE             set to 'yes' or 'true' to get a relative URI");
    }

    /**
     * Create a {@code NiUri} object by hashing a file.
     * @param filepath Path to the file to be hashed.
     * @param algo {@code sha-256}, {@code sha-384} and {@code sha-512} are
     * currently supported if the JRE supports all of them. {@code sha-256} is
     * guaranteed to be supported.
     * @param authority Authority part of the URI. If in doubt, provide the
     * empty string here.
     * @return
     * @throws IOException if an error occurrs while reading the file
     * @throws NoSuchAlgorithmException if an unsupported algorithm is provided
     */
    public static NiUri ofFile(String filepath, String algo, String authority) throws IOException, NoSuchAlgorithmException {

        MessageDigest md = NiUri.hasherFromAlgorithmName(algo);

        // file hashing with DigestInputStream
        try (DigestInputStream dis = new DigestInputStream(new FileInputStream(filepath), md)) {
            while (dis.read() != -1) ; //empty loop to clear the data
            md = dis.getMessageDigest();
        }

        return new NiUri(algo.toLowerCase(), md.digest(), authority);
    }

    /**
     * Create a {@code NiUri} object by hashing a file.
     * @param filepath Path to the file to be hashed.
     * @param algo {@code sha-256}, {@code sha-384} and {@code sha-512} are
     * currently supported if the JRE supports all of them. {@code sha-256} is
     * guaranteed to be supported.
     * @return
     * @throws IOException if an error occurrs while reading the file
     * @throws NoSuchAlgorithmException if an unsupported algorithm is provided
     */
    public static NiUri ofFile(String filepath, String algo) throws IOException, NoSuchAlgorithmException {
        return NiUri.ofFile(filepath, algo, "");
    }

    /**
     * Create a {@code NiUri} object by hashing a file.
     * @param filepath Path to the file to be hashed.
     * @return
     * @throws IOException if an error occurrs while reading the file
     * @throws NoSuchAlgorithmException if {@code sha-256} is not supported
     */
    public static NiUri ofFile(String filepath) throws IOException, NoSuchAlgorithmException {
        return NiUri.ofFile(filepath, "sha-256");
    }

    /**
     * Create a {@code NiUri} object by hashing the UTF-8 representation of a String.
     * @param str String to be hashed.
     * @param algo {@code sha-256}, {@code sha-384} and {@code sha-512} are
     * currently supported if the JRE supports all of them. {@code sha-256} is
     * guaranteed to be supported.
     * @param authority Authority part of the URI. If in doubt, provide the
     * empty string here.
     * @return
     * @throws NoSuchAlgorithmException if an unsupported algorithm is provided
     */
    public static NiUri ofString(String str, String algo, String authority) throws NoSuchAlgorithmException {
        MessageDigest md = NiUri.hasherFromAlgorithmName(algo);
        return new NiUri(algo.toLowerCase(), md.digest(str.getBytes(StandardCharsets.UTF_8)), authority);
    }

    /**
     * Create a {@code NiUri} object by hashing the UTF-8 representation of a String.
     * @param str String to be hashed.
     * @param algo {@code sha-256}, {@code sha-384} and {@code sha-512} are
     * currently supported if the JRE supports all of them. {@code sha-256} is
     * guaranteed to be supported.
     * @return
     * @throws NoSuchAlgorithmException if an unsupported algorithm is provided
     */
    public static NiUri ofString(String str, String algo) throws NoSuchAlgorithmException {
        return NiUri.ofString(str, algo, "");
    }

    /**
     * Create a {@code NiUri} object by hashing the UTF-8 representation of a String.
     * @param str String to be hashed.
     * @return
     * @throws NoSuchAlgorithmException if {@code sha-256} is not supported
     */
    public static NiUri ofString(String str) throws NoSuchAlgorithmException {
        return NiUri.ofString(str, "sha-256");
    }

    /**
     * Create a {@code NiUri} object from a ni URI
     * @param uriString
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static NiUri ofUri(String uriString) throws NoSuchAlgorithmException {
        URI uri = URI.create(uriString);
        String scheme = uri.getScheme();
        String algo;
        byte[] rawHash;
        switch (scheme) {
            case "ni":
                String authority = uri.getAuthority();
                String path = uri.getPath().replaceFirst("/", "");
                String[] pathParts = path.split(";");
                algo = pathParts[0];
                String hash = pathParts[1];
                rawHash = Base64.getUrlDecoder().decode(hash);
                return new NiUri(algo, rawHash, authority);
            case "nih":
                String[] nihParts = uri.getSchemeSpecificPart().split(";");
                algo = nihParts[0];
                switch (nihParts[0]) {
                    case "1":
                        algo = "sha-256";
                        break;
                    case "7":
                        algo = "sha-384";
                    case "8":
                        algo = "sha-512";
                }
                String hexString = nihParts[1].replace("-", "");
                rawHash = hexToByteArray(hexString);
                if(nihParts.length == 3) {
                    // check digit
                    String foundCheckDigit = nihParts[2];
                    String expectedCheckDigit = luhnModN(hexString, "0123456789abcdef");
                    if(!foundCheckDigit.equals(expectedCheckDigit)) {
                        throw new RuntimeException("Check digit mismatch");
                    }
                }
                return new NiUri(algo, rawHash, "");
            default:
                throw new RuntimeException("Unsupported URI scheme: " + scheme);
        }
    }

    /**
     * Helper method that returns the corresponding {@link MessageDigest} for
     * a given algorithm name as defined in RFC 6920.
     * @param algoName The algorithm name as defined in RFC 6920.
     * @return
     * @throws NoSuchAlgorithmException if an unsupported algorithm is provided
     */
    public static MessageDigest hasherFromAlgorithmName(String algoName) throws NoSuchAlgorithmException {
        return switch (algoName.toLowerCase()) {
            case "sha-256" -> MessageDigest.getInstance("SHA-256");
            case "sha-384" -> MessageDigest.getInstance("SHA-384");
            case "sha-512" -> MessageDigest.getInstance("SHA-512");
            default -> throw new NoSuchAlgorithmException("Unsupported algorithm: " + algoName.toLowerCase());
        };
    }

    /**
     * Helper method to turn a byte array into a hexadecimal String.
     * @param a The byte array.
     * @return
     */
    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static byte[] hexToByteArray(String hexString) {
        if (hexString.length() % 2 == 1) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length(); i += 2) {
            bytes[i / 2] = hexToByte(hexString.substring(i, i + 2));
        }
        return bytes;
    }

    private static byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }

    private static int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }

    /**
     * Helper method to insert dashes at regular intervals into a String.
     * @param str The String the dashes are to be inserted into.
     * @param dashesEvery The number of characters after which a dash is to be
     * inserted.
     * @return
     */
    public static String insertDashes(String str, int dashesEvery) {
        String withDashes = str.substring(0, dashesEvery);
        for(int i = dashesEvery; i < str.length(); i += dashesEvery) {
            withDashes += "-" + str.substring(i, i + dashesEvery);
        }
        return withDashes;
    }

    /**
     * Calculate the Luhn-mod-N check digit of a String. The String must not
     * include a check digit already.
     * @param s The String for which to calculate the check digit. Must only
     * contain characters from the given alphabet.
     * @param alphabet The alphabet to use.
     * @return
     */
    public static String luhnModN(String s, String alphabet) {
        int count = 0;
        int luhnVal = 0;
        int multiplyOn = (s.length() +1) % 2;
        int alphabetLength = alphabet.length();
        for(char c : s.toCharArray()){
            int i = alphabet.indexOf(c);

            if(count %2 == multiplyOn) {
                i *= 2;
            }

            i = (i / alphabetLength) + (i % alphabetLength);
            luhnVal = (luhnVal + i) % alphabetLength;
            count += 1;
        }
        luhnVal = (alphabetLength - (luhnVal % alphabetLength)) % alphabetLength;
        return String.valueOf(alphabet.charAt(luhnVal));
    }

    public static final String[] SUPPORTED_ALGORITHMS = {
            "sha-256",
            "sha-384",
            "sha-512"
    };

    private final String algo;
    private final byte[] digest;
    private final String authority;

    public String getAlgo() {
        return algo;
    }

    public byte[] getDigest() {
        return digest;
    }

    public String getAuthority() {
        return authority;
    }

    private NiUri(String algo, byte[] digest, String authority) throws NoSuchAlgorithmException {
        if(!Arrays.asList(SUPPORTED_ALGORITHMS).contains(algo)){
            throw new NoSuchAlgorithmException("Unsupported algorithm: " + algo);
        }
        String[] algoParts = algo.split("-");
        int algoLength = Integer.parseInt(algoParts[algoParts.length -1]);
        if(digest.length != algoLength / 8) {
            throw new RuntimeException("Digest length does not match the specified algorithm");
        }
        this.algo = algo;
        this.authority = authority;
        this.digest = digest;
    }

    /**
     * Returns a {@code ni://} URI as a String and enforces the given
     * {@code authority} to be used.
     * @param overrideAuthority The {@code authority} to be used in the URI.
     * @param relative set to {@code true} to create a relative URI String
     * @return
     */
    public String format(String overrideAuthority, boolean relative) {
        String relativeUri = this.algo + ";" + Base64.getUrlEncoder().encodeToString(this.digest).replaceAll("=", "");
        if(relative) {
            return relativeUri;
        }
        return "ni://" + overrideAuthority + "/" + relativeUri;
    }

    /**
     * Returns a {@code ni://} URI as a String and enforces the given
     * {@code authority} to be used.
     * @param overrideAuthority The {@code authority} to be used in the URI.
     * @return
     */
    public String format(String overrideAuthority) {
        return this.format(overrideAuthority, false);
    }

    /**
     * Returns a {@code ni://} URI as a String.
     * @return
     */
    public String format() {
        return this.format(this.authority);
    }

    /**
     * Returns a {@code ni://} URI as a String.
     * @param relative set to {@code true} to create a relative URI String
     * @return
     */
    public String format(boolean relative) {
        return this.format(this.authority, relative);
    }

    /**
     * Returns a {@code nih:} URI as a String, including a Luhn-mod-N check
     * digit. If possible, the algorithm name is abbreviated to a decimal code.
     * {@code nih:} URIs only consist of hexadecimal characters and are
     * considered human-readable in the RFC.
     * @param relative set to {@code true} to create a relative URI String
     * @return
     */
    public String formatNih(boolean relative) {
        // FIXME: Luhn-mod-N check digit
        String algoCode = this.algo;
        switch(this.algo) {
            case "sha-256":
                algoCode = "1";
                break;
            case "sha-384":
                algoCode = "7";
                break;
            case "sha-512":
                algoCode = "8";
                break;
        }
        String hexDigest = NiUri.byteArrayToHex(this.digest);
        String checkDigit = NiUri.luhnModN(hexDigest, "0123456789abcdef");
        String hexDigestDashed = NiUri.insertDashes(hexDigest, 4);
        String relativeUri = algoCode + ";" + hexDigestDashed + ";" + checkDigit;
        if(relative) {
            return relativeUri;
        }
        return "nih:" + relativeUri;
    }

    /**
     * Returns a {@code nih:} URI as a String, including a Luhn-mod-N check
     * digit. If possible, the algorithm name is abbreviated to a decimal code.
     * {@code nih:} URIs only consist of hexadecimal characters and are
     * considered human-readable in the RFC.
     * @return
     */
    public String formatNih() {
        return this.formatNih(false);
    }

    /**
     * Returns a well-known URL as a String with the given protocol and enforces the given
     * {@code authority} to be used.
     * @param protocol
     * @param overrideAuthority
     * @return
     */
    public String formatWellKnown(String protocol, String overrideAuthority) {
        if(overrideAuthority == null || "".equals(overrideAuthority)) {
            throw new RuntimeException("Authority cannot be empty");
        }
        String path = "/.well-known/ni/" + this.algo + "/" + Base64.getUrlEncoder().encodeToString(this.digest).replaceAll("=", "");
        try {
            return (new URI(protocol, overrideAuthority, path, null, null)).toString();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Returns a well-known URL as a String with the given protocol.
     * @param protocol
     * @return
     */
    public String formatWellKnown(String protocol) {
        return formatWellKnown(protocol, this.authority);
    }

    @Override
    public String toString() {
        return this.format();
    }
}
